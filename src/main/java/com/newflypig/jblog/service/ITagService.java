package com.newflypig.jblog.service;

import java.util.List;

import com.newflypig.jblog.model.Tag;

public interface ITagService extends IBaseService<Tag>{
	
	/**
	 * 根据title查找tag
	 * @param tagTitle
	 * @return
	 */
	Tag findByTitle(String tagTitle);
	
	/**
	 * 对tag和article构建多对多关系，在多对多表中建立连接
	 * @param tagId
	 * @param articleId
	 */
	void makeConnect(Integer tagId, Integer articleId);
	
	/**
	 * 清空tags多对多关系
	 * @param articleId
	 */
	void cleanConnect(Integer articleId);
	
	/**
	 * 为sitemap提供一份url列表，只需要简单的title
	 * @return
	 */
	List<String> findTagUrls();
	
	/**
	 * 从实体表中检索title字符串数组
	 * @return
	 */
	List<String> findAllFromTable();
}
